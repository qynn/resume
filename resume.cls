\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{resume}[2021 2ynn]

%%%% GEOMETRY %%%
\LoadClass{article}
\RequirePackage[total={8.5in, 11in}, top=0.3in, outer=0.3in, inner=0.3in]{geometry}

%%%% BASICS %%%
%\RequirePackage{pdfsync}
\RequirePackage[english]{babel}
\RequirePackage{lipsum}
%\RequirePackage[]{inputenc}  % for accents (only w/ pdfLatex?)
\RequirePackage{amssymb}
\RequirePackage{amsfonts}
\RequirePackage{amsmath}
\RequirePackage{color}
\RequirePackage{enumitem}
%\RequirePackage{verbatim} %C++

%%%% SPACINGS %%%
\RequirePackage{calc}
\RequirePackage{setspace}

%Vertical
\setlength{\parindent}{0pt}
%\renewcommand{\baselinestretch}{1.0}  %set vertical spacing
\newcommand{\lb}{\linebreak}
%Skips
\newcommand{\uskip}{40pt} % huge skip\lb
\newcommand{\bskip}{30pt} % big skip
\newcommand{\dskip}{24pt} % double skip
\newcommand{\fskip}{18pt} % one and a half skip
\newcommand{\nskip}{12pt} % normal skip
\newcommand{\sskip}{6pt} % small skip
\newcommand{\tskip}{2pt} % tiny skip
\newcommand{\skeep}{\lb\\[-\sskip]} % hack skip

%Horizontal
% NB: \tabcolsep is inserted at the right of L and at the left of Z
\setlength{\tabcolsep}{0.075in}  % separation between major columns
\newcommand{\colwidth}{2.25in} %

\RequirePackage[colorlinks=true,linkcolor=red,urlcolor=blue]{hyperref}
%%%% LINKS %%%%
\RequirePackage{attachfile}
\newcommand{\blink}[2]{\href{#2}{\color{blue}{#1}}}

%%%% TABLES %%%%
\RequirePackage{tabularx}
%\RequirePackage{multirow}
%\RequirePackage{multicol}
\RequirePackage{pbox}  % linebreak in tabularx
\RequirePackage{array}

% @{} is used to flush left or right
\newcolumntype{Y}{@{}>{\raggedright\arraybackslash}X}
\newcolumntype{Z}{>{\raggedright\arraybackslash}X@{}}
\newcolumntype{C}{@{}>{\raggedright\arraybackslash}X@{}}
\newcolumntype{L}[1]{@{}>{\raggedright\arraybackslash}p{#1}}
\newcolumntype{R}[1]{>{\raggedright\arraybackslash}p{#1}@{}}

%%%% FONTS %%%%
\RequirePackage{fontspec} %W!requires xelatex or lualatex !!
\setmainfont{DejaVuSans.ttf}[
BoldFont		=	{DejaVuSansMono-Bold.ttf}, %textbf{}
ItalicFont		=	{DejaVuSans-Oblique.ttf}, %textit{}
BoldItalicFont	=	{DejaVuSansMono-BoldOblique.ttf}, %textbfit{}
SmallCapsFont	=	{DejaVuSerif-Italic.ttf},  %textsc{}
]
\setmonofont{DejaVuSansMono.ttf} % texttt{}
\newcommand{\lmr}{\fontfamily{lmr}\selectfont} % switch back to Latin Modern Roman
\newcommand{\namefont}[1]{\textbf{\fontsize{24}{24}\selectfont{\addfontfeature{LetterSpace=20.0}#1}}}

%%%% SHORTCUTS %%%%
\newcommand{\quot}[1]{\texttt{'}#1\texttt{'}}  %quote unquote (single)
\newcommand{\pr}[1]{(\,#1\,)} %parentheses with Thin spaces
\newcommand{\mg}[1]{{\color{magenta}\texttt{#1}}}


%%%% LAYOUT BLOCKS %%%%

\newcommand{\hblock}[1]{ %header block (1)
  \begin{tabularx}{\textwidth}{L{\colwidth}L{\colwidth+\tabcolsep}C}
    #1
  \end{tabularx}
}

\newcommand{\lblock}[1]{ %left block (1/3)
  \vspace{-\tskip}
  {\begin{tabularx}{\linewidth}{C}
      #1
    \end{tabularx}}
}

\newcommand{\rblock}[1]{ %right block (2/3)
  \vspace{-\tskip}
  {\begin{tabularx}{\linewidth}{L{\colwidth}C}
      #1
    \end{tabularx}}
}


% -%-%-%-%-%-%-% CV SPECIFIC COMMANDS %-%-%-%-%-%-%-%

%%%% CATEGORIES %%%%
\newcommand{\Cat}[1]{
	{ {\fontsize{18}{24}\selectfont\textbf{#1}} \strut
		\vspace{\tskip} {\color{magenta}\hrule height 2pt width \linewidth} \vspace{\nskip}}
	% \vspace{0.2\baselineskip} \hrule height 2pt width \linewidth \vspace{10pt}}
	% \vspace{\dimexpr \sskip-\baselineskip\relax} \hrule height 2pt width \linewidth \vspace{\nskip}}
}

%%%% TITLES %%%%
\newcommand{\bfl}[1]{\textbf{\large #1}} %Lage& Bold for titles
\newcommand{\ttl}[1]{\bfl{#1}\\[\tskip]} % Title with Return Skip
\newcommand{\tts}[1]{\textbf{#1}\\[\tskip]}

%%%% EXPERIENCE %%%%
\newcommand{\expEntry}[4]{ %{Ttile}{When}{Where}{What}
	{\begin{tabular}[t]{Y}
     \bfl{#1}\\[-\tskip]
     {\color{magenta} {\small #2}}\\[\nskip]
     \textit{\small #3}
   \end{tabular}} & {\small #4} \\[\sskip]
}

\newcommand{\expTask}[2]{ %{Title}{Description}
  \textbf{#1}\lb
  {#2}\vspace{4pt}\lb
}

%%%% PUBLICATIONS %%%%
\newcommand{\subCat}[1]{\bfl{#1}\\[\sskip]} %for Publications

%%%% MISC %%%%
\newcommand{\cpp}{C{\small++}} % C/C++
\newcommand{\ccpp}{C\,/\ \cpp} % C/C++
